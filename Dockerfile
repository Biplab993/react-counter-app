FROM node:14.17.1-alpine3.12

WORKDIR /home/node/app
COPY . .

EXPOSE 3000
ENTRYPOINT ["npm","start"] 